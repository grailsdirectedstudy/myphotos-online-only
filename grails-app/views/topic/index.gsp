<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos - Topics</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "col-sm-10 col-sm-offset-1">
                <h1 id = "serverTopicsHeading">Topics Stored on Server</h1>
                <mp:topicCollection />
            </div>
        </div> <!-- Container -->

        <asset:javascript src = "util.js" />
        <asset:javascript src = "topic.js" /> 
        <asset:javascript src = "viewTopics.js" />
    </body>
</html>
