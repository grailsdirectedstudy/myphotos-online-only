<!DOCTYPE html>
<html>
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
        <g:render template="/assets" />
    </head>
    <body>
        <g:render template="/navbar" />
        <g:layoutBody/>
        <br /><br /> <!-- Add some whitespace at the bottom -->
    </body>
</html>