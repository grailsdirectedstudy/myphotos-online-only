#Complete Online myPhotos App

This is a Grails app that is built as a starting point for the offline-enabled apps (myPhotos-complete and ngPhotos).

See: 
    
    https://bitbucket.org/grailsdirectedstudy/myphotos-complete
    https://bitbucket.org/grailsdirectedstudy/ngphotos